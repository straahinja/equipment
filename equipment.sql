-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2020 at 04:54 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `equipment`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(4) NOT NULL,
  `email` varchar(50) NOT NULL,
  `lozinka` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `lozinka`) VALUES
(1, 'stevan.radic@underit.rs', 'underit'),
(2, 'strahinjadrinic@yahoo.com', 'underit'),
(3, 'dusan.markovic@underit.rs', 'underit'),
(4, 'sladjan.jordakovic@underit.rs', 'underit');

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(4) NOT NULL,
  `ime` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `lozinka` varchar(25) NOT NULL,
  `statuss` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `ime`, `email`, `lozinka`, `statuss`) VALUES
(1, 'Novak Josić', 'novak.josic@underit.rs', 'default', 'active'),
(2, 'Miodrag Tomić', 'miodrag.tomic@underit.rs', 'default', 'active'),
(3, 'Nebojša Ognjanović', 'nebojsa.ognjanovic@underit.rs', 'default', 'active'),
(4, 'Nenad Nikolić', 'nenad.nikolic@underit.rs', 'default', 'active'),
(5, 'Slađan Jordaković', 'sladjan.jordakovic@underit.rs', 'default', 'active'),
(6, 'Strahinja Drinić', 'strahinjadrinic@yahoo.com', 'default', 'active'),
(7, 'Dušan Marković', 'dusan.markovic@underit.rs', 'default', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `odobrenizahtevi`
--

CREATE TABLE `odobrenizahtevi` (
  `br_zahteva` int(4) NOT NULL,
  `idkorisnika` int(4) NOT NULL,
  `imeip` varchar(35) NOT NULL,
  `email` varchar(35) NOT NULL,
  `barkod` bigint(11) NOT NULL,
  `statuss` varchar(35) NOT NULL,
  `datumivreme` varchar(20) NOT NULL,
  `naziv` varchar(50) NOT NULL,
  `requestedfrom` varchar(40) NOT NULL,
  `requestedfromuser` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `odobrenizahtevi`
--

INSERT INTO `odobrenizahtevi` (`br_zahteva`, `idkorisnika`, `imeip`, `email`, `barkod`, `statuss`, `datumivreme`, `naziv`, `requestedfrom`, `requestedfromuser`) VALUES
(1, 6, 'Dušan Marković', 'dusan.markovic@underit.rs', 214345432543, 'assigned', '29.06.2020/00:45', 'Asus VP228DE TN', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `predmeti`
--

CREATE TABLE `predmeti` (
  `barkod` bigint(11) NOT NULL,
  `naziv` varchar(50) NOT NULL,
  `statuss` varchar(50) NOT NULL,
  `descriptionn` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `predmeti`
--

INSERT INTO `predmeti` (`barkod`, `naziv`, `statuss`, `descriptionn`) VALUES
(231231984789, 'dellx33', 'free', 'nesto'),
(234823984789, 'dell', 'free', 'stolica');

-- --------------------------------------------------------

--
-- Table structure for table `zahtevi`
--

CREATE TABLE `zahtevi` (
  `br_zahteva` int(4) NOT NULL,
  `idkorisnika` int(4) NOT NULL,
  `imeip` varchar(40) NOT NULL,
  `email` varchar(35) NOT NULL,
  `naziv` varchar(50) NOT NULL,
  `barkod` bigint(12) NOT NULL,
  `statuss` varchar(25) NOT NULL,
  `datumivreme` varchar(20) NOT NULL,
  `info` varchar(255) NOT NULL,
  `comment_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `odobrenizahtevi`
--
ALTER TABLE `odobrenizahtevi`
  ADD PRIMARY KEY (`br_zahteva`);

--
-- Indexes for table `predmeti`
--
ALTER TABLE `predmeti`
  ADD PRIMARY KEY (`barkod`);

--
-- Indexes for table `zahtevi`
--
ALTER TABLE `zahtevi`
  ADD PRIMARY KEY (`br_zahteva`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `zahtevi`
--
ALTER TABLE `zahtevi`
  MODIFY `br_zahteva` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
