<!DOCTYPE html>
<html lang="en">

<head>
    <title>Calendar</title>
    <meta charset="utf-8">

    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet"
        href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/css/datepicker.css">
    <link rel="stylesheet" href="https://cdn.webix.com/edge/webix.css">

    <!--Author-->
    <link rel="stylesheet" href="timer_test.css">
</head>

<body>


    <div>
        <ul style="width: 20%; height: auto; margin: 0 auto; float: left; margin-top: 100px; margin-left: 50px;" class="list-group">
            <center> <li class="list-group-item active">Month summary</li> </center>
            <li class="list-group-item">Work Days:</li>
            <li class="list-group-item">Entered Days:</li>
            <li class="list-group-item">Measured presence(h):</li>
            <li class="list-group-item">Estimated presence(h):</li>
            <li class="list-group-item">Entered presence(h):</li>
            <li class="list-group-item">Overtime hours:</li>
        </ul>
    </div>

    <div style="width: ; height: auto; margin: 0 auto; float: right; margin-top: 100px; margin-right: 50px;" id="areaA"></div>



    <div style="width: 40%; height: auto; margin: 0 auto;" class="p-5" >
        <!-- type: date or month -->
        <label style="font-weight: bold;" for="start">Quick Search</label>
        <input type="date" id="start" name="start"min="" value="">
        <!-- type: date or month END -->
        <h2 class="mb-4">My Calendar</h2>
        <div style="" class="card">
            <div class="card-body p-0">
                <div id="calendar"></div>
                
            </div>
            
        </div>

        <!--Tabs-->

    <!-- calendar modal -->
    <div  id="modal-view-event" class="modal modal-top fade calendar-modal">
        <div  class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="modal-title"><span class="event-icon"></span><span class="event-title"></span></h4>
                    <div class="event-body"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-view-event-add" class="modal modal-top fade calendar-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form id="add-event">
                    <div class="modal-body">
                        <center>
                            <h4>Enter Data</h4>
                        </center>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Projects
                            </button>

                            <!-- Dodati Js za dropdown-->
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">MediaBiro</a>
                                <a class="dropdown-item" href="#">Support</a>
                                <a class="dropdown-item" href="#">eQuipment</a>
                                <a class="dropdown-item" href="#">Kara</a>
                                <a class="dropdown-item" href="#">Kara2</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <input type="text" class="form-control" name="ename">
                        </div>
        
                        <div class="time-picker" data-time="00:00">
		<div class="hour">
			<div class="hr-up"></div>
			<input type="number" class="hr" value="00" />
			<div class="hr-down"></div>
		</div>

		<div class="separator">:</div>

		<div class="minute">
			<div class="min-up"></div>
			<input type="number" class="min" value="00">
			<div class="min-down"></div>
		</div>
	</div>

                        <div class="form-group">
                            <label>Description</label>
                            <textarea style="height:100px;" class="form-control" name="edesc"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Work Type</label>
                            <select class="form-control" name="ecolor">
                                <!-- <option value="fc-bg-default">Default</option> -->
                                <option value="fc-bg-lightgreen">Office</option>
                                <option value="fc-bg-blue">Home</option>
                                <option value="fc-bg-pinkred">/</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">


                    
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
        
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.en.js"></script>
    <script src="https://cdn.webix.com/edge/webix.js"></script>
    <!--Author-->
    <script src="timer_test.js"></script>


<script>
        function submit() {
    webix.message(JSON.stringify($$("myForm").getValues(), null, 2));
    }

    webix.ui({
    view: "form",
    id: "myForm",
    container: "areaA",
    width: 450,
    elements: [{
        view: "tabview",
        height: 250,
        cells: [{
        header: "<button class='webixbutton'>Godisnji</button>",
        body: {
            rows: [{}, {
            view: "text",
            label: "Name",
            name: "name"
            }, {
            view: "datepicker",
            label: "Birthdate",
            id: "bdate",
            name: "bdate",
            stringResult: "true",
            editable: "true",
            placeholder: "MM/DD/YYYY"
            }, {
            view: "text",
            label: "Address",
            name: "address"
            }, {
            view: "text",
            label: "Phone",
            name: "phone"
            }, {
            view: "text",
            label: "Email",
            name: "email"
            }, ]
        }
        }, {
        header: "<button class='webixbutton'>Bolovanje</button>",
        body: {
            rows: [{}, {
            view: "text",
            label: "Card №",
            name: "cardnum"
            }, {
            view: "text",
            label: "Packaging",
            name: "packaging"
            }, {
            view: "datepicker",
            label: "Date",
            name: "deldate",
            stringResult: "true",
            timepicker: "true",
            editable: "true",
            placeholder: "MM/DD/YYYY HH:MM (AM/PM)"
            }, {
            view: "textarea",
            label: "Comment",
            name: "comment"
            }, ]
        }
        }]
    }, {
        view: "button",
        value: "Submit",
        width: 150,
        align: "center",
        click: submit
    }]
    });

    webix.UIManager.addHotKey("enter", function(view) {
    view.getPopup().show(view.getInputNode());
    }, "datepicker");
</script>

</body>

</html>