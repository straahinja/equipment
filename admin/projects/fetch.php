<?php
//fetch.php;
if(isset($_POST["view"]))
{
 include("connection.php");
 if($_POST["view"] != '')
 {
  $update_query = "UPDATE zahtevi SET comment_status=1 WHERE comment_status=0";
  mysqli_query($conn, $update_query);
 }
 $query = "SELECT * FROM zahtevi ORDER BY br_zahteva DESC LIMIT 9";
 $result = mysqli_query($conn, $query);
 $output = '';
 
 if(mysqli_num_rows($result) > 0)
 {
  while($row = mysqli_fetch_array($result))
  {
   $output .= '
   <li>
    <a href="http://localhost/equipment/admin/equipmentrequests.php">
     <strong>'.$row["imeip"].' requested an item</strong><br />
     <small><em>'.$row["naziv"].'</em></small>
    </a>
   </li>
   <li class="divider"></li>
   ';
  }
 }
 else
 {
  $output .= '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
 }
 
 $query_1 = "SELECT * FROM zahtevi WHERE comment_status=0";
 $result_1 = mysqli_query($conn, $query_1);
 $count = mysqli_num_rows($result_1);
 $data = array(
  'notification'   => $output,
  'unseen_notification' => $count
 );
 echo json_encode($data);
}
?>
