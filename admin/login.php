<?php
  session_start();

?>

<?php
    error_reporting( E_ALL & ~E_NOTICE ^ E_DEPRECATED );
    
?>

<?php

  if(isset($_POST['prijavise'])){
      $email = $_POST['email'];
      $lozinka = $_POST['lozinka'];

    require('connection.php');
      $sql = "SELECT * FROM admin WHERE email='".$email."' AND lozinka='".$lozinka."'";
      // if(mysqli_connect($host, $server_username, $server_pass,$base ))
        if($conn)  
              $x = mysqli_query($conn, $sql);
              if(mysqli_num_rows($x) == 1){

                  $_SESSION['admin'] = $email;
                  header('Location: index.php');
              } 
              else {
                 $greska="Check your input again!" ;
              }
         }
?>




<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin - Login</title>
  <link rel="icon" 
      type="image/png" 
      href="https://img.icons8.com/dotty/80/000000/admin-settings-male.png">
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div style="margin-top: 100px;" class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                    <label>Version 1.0</label>
                  </div>
                  <form method="POST"  class="user">
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Admin Email" value="<?php echo $_POST['email'] ?? ''; ?>">
                    </div>
                    <div class="form-group">
                      <input type="password" name="lozinka" class="form-control form-control-user" id="exampleInputPassword" placeholder="Admin Password">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        
                      </div>
                      
                    </div>
                    
                    <button  class="btn btn-primary btn-user btn-block" name="prijavise">Login</button>
                    
                    <!-- <hr> -->
                    <!-- <a href="index.html" class="btn btn-google btn-user btn-block">
                     <i class="fab fa-google fa-fw"></i> Login with Google
                    </a> -->
                    <br><br><center><label style="color:red; font-weight:bold;"><?php echo $greska; ?></label></center><br><br><br><br>
                    
                  </form>
                  <!-- <hr> -->
                  <div class="text-center">
                    
                  </div>
                  <div class="text-center">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
