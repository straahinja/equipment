<?php

require('render1.php');

?>

<link rel="stylesheet" href="css/pagination.css">

<div class="SearchMainDiv"><br>
         <div class="SearchByBarcode">
        <form class="d-flex justify-content-center" method="POST" action="findbar.php">
        <input class="d-flex justify-content-center" type="text" placeholder="Insert Barcode" aria-label="Search" name="search">
       <center> <button class="btn blue-gradient btn-rounded btn-sm my-0 border ml-1" type="submit" name="nadji">Search</button>
        </form>
    </div><br>

    <div class="SearchByName">
        <form class="d-flex justify-content-center" method="POST" action="nadjinaziv.php">
        <input class="d-flex justify-content-center" type="text" placeholder="Name Of Equipment" aria-label="Search" name="search">
        <button class="btn blue-gradient btn-rounded btn-sm my-0 border ml-1" type="submit" name="nadji">Search</button>
        </form><br>
    </div>
</div>
<br><center><label>Availabel Items</label></center><br>



<link rel="stylesheet" href="css/pagination.css">
<div class="table-responsive">
  <table class="table table-bordered text-nowrap" style='background-color: white' class='table table-bordered'>
  <thead>
  <tr>
  <th style='width:50px;'>Barcode</th>
  <th style='width:150px;'>Name</th>
  <th style='width:50px;'>Status</th>
  <!-- <th style='width:150px;'>Department</th> -->
  </tr>
  </thead>
  <tbody>
</div>
<?php
include('connection.php');

if (isset($_GET['page_no']) && $_GET['page_no']!="") {
	$page_no = $_GET['page_no'];
	} else {
		$page_no = 1;
        }

	$total_records_per_page = 4;
    $offset = ($page_no-1) * $total_records_per_page;
	$previous_page = $page_no - 1;
	$next_page = $page_no + 1;
	$adjacents = "2"; 

	$result_count = mysqli_query($conn,"SELECT COUNT(*) As total_records FROM `predmeti`");
	$total_records = mysqli_fetch_array($result_count);
	$total_records = $total_records['total_records'];
    $total_no_of_pages = ceil($total_records / $total_records_per_page);
	$second_last = $total_no_of_pages - 1; // total page minus 1

    $result = mysqli_query($conn,"SELECT * FROM `predmeti` LIMIT $offset, $total_records_per_page");
    while($row = mysqli_fetch_array($result)){
		echo "<tr>
			  <td>".$row['barkod']."</td>
			  <td>".$row['naziv']."</td>
        <td>".$row['statuss']."</td>
		   	  
		   	  </tr>";
        }
	mysqli_close($conn);
    ?>
</tbody>
</table>


<div style='padding: 10px 20px 0px; border-top: dotted 1px #CCC;'>
<strong>Page <?php echo $page_no." of ".$total_no_of_pages; ?></strong>
</div>

<ul class="pagination">
	<?php // if($page_no > 1){ echo "<li><a href='?page_no=1'>First Page</a></li>"; } ?>
    
	<li <?php if($page_no <= 1){ echo "class='disabled'"; } ?>>
	<a <?php if($page_no > 1){ echo "href='?page_no=$previous_page'"; } ?>>Previous</a>
	</li>
       
    <?php 
	if ($total_no_of_pages <= 10){  	 
		for ($counter = 1; $counter <= $total_no_of_pages; $counter++){
			if ($counter == $page_no) {
		   echo "<li class='active'><a>$counter</a></li>";	
				}else{
           echo "<li><a href='?page_no=$counter'>$counter</a></li>";
				}
        }
	}
	elseif($total_no_of_pages > 10){
		
	if($page_no <= 4) {			
	 for ($counter = 1; $counter < 8; $counter++){		 
			if ($counter == $page_no) {
		   echo "<li class='active'><a>$counter</a></li>";	
				}else{
           echo "<li><a href='?page_no=$counter'>$counter</a></li>";
				}
        }
		echo "<li><a>...</a></li>";
		echo "<li><a href='?page_no=$second_last'>$second_last</a></li>";
		echo "<li><a href='?page_no=$total_no_of_pages'>$total_no_of_pages</a></li>";
		}

	 elseif($page_no > 4 && $page_no < $total_no_of_pages - 4) {		 
		echo "<li><a href='?page_no=1'>1</a></li>";
		echo "<li><a href='?page_no=2'>2</a></li>";
        echo "<li><a>...</a></li>";
        for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {			
           if ($counter == $page_no) {
		   echo "<li class='active'><a>$counter</a></li>";	
				}else{
           echo "<li><a href='?page_no=$counter'>$counter</a></li>";
				}                  
       }
       echo "<li><a>...</a></li>";
	   echo "<li><a href='?page_no=$second_last'>$second_last</a></li>";
	   echo "<li><a href='?page_no=$total_no_of_pages'>$total_no_of_pages</a></li>";      
            }
		
		else {
        echo "<li><a href='?page_no=1'>1</a></li>";
		echo "<li><a href='?page_no=2'>2</a></li>";
        echo "<li><a>...</a></li>";

        for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
          if ($counter == $page_no) {
		   echo "<li class='active'><a>$counter</a></li>";	
				}else{
           echo "<li><a href='?page_no=$counter'>$counter</a></li>";
				}                   
                }
            }
	}
?>
    
	<li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
	<a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page'"; } ?>>Next</a>
	</li>
    <?php if($page_no < $total_no_of_pages){
		echo "<li><a href='?page_no=$total_no_of_pages'>Last &rsaquo;&rsaquo;</a></li>";
		} ?>
</ul>

</div>

<?php

  //require('requesteditem.php');
  require('render2.php');
?> 

<!--           __
             <(o )___
              ( ._> /
               `---' 
  -->