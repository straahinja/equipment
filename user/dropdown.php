<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
        <title></title>

        <style>
            input[type=text] {
            width: 50%;
            padding: 12px 20px;
            margin: 8px 0;
           box-sizing: border-box;
            border: 3px solid #ccc;
            -webkit-transition: 0.5s;
            transition: 0.5s;
            outline: none;
            }

            input[type=text]:focus {
            border: 3px solid #555;
            }
        </style>

        <script>
            function ddlselect () {
                var d= document.getElementById("ddselect");
                var displaytext=d.options[d.selectedIndex].text;
                document.getElementById("txtvalue").value=displaytext;

                
            }
        </script> 
    </head>

    <body>
        <center>
            <select id="ddselect" onchange="ddlselect();">
                <option>--Select--</option>
                <option>Glory</option>
                <option>Annual</option>
                <option>Sick Benefit</option>
                <option>Birth</option>
                <option>Moving</option>
                <option>Death</option>
            </select>
            <input style="width:50%;" type="text" id="txtvalue" value=""/>
        </center>

         <center>   

         
            <form>
                <label for="razlog">Razlog</label>
                <input type="text" id="razlog" name="razlog" value="">
            </form>
        </center>

    </body>

</html>