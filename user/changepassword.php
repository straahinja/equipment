<?php
    session_start();
    if(!isset($_SESSION['em'])){
        header('Location: login.php');
    }
?>

<?php
    error_reporting( E_ALL & ~E_NOTICE ^ E_DEPRECATED );
    
?>
<?php
require('connection.php');
    if(isset($_POST['submitnewpassword'])){
        $newpass=$_POST['newpassword'];
        $confirmnewpass=$_POST['confirmnewpassword'];
        $error="";
        if(empty($newpass)|| $confirmnewpass=="")
        {
            $error='All fields are required!';
        }
        else if($_POST['newpassword'] != $_POST['confirmnewpassword'])
        {
            $error2='Passwords do not match!';
        }
        if(empty($error || $error2)){
            $query="UPDATE korisnici SET lozinka= '$confirmnewpass' WHERE email='".$_SESSION['em']."'";
            $result=mysqli_query($conn, $query);
            if($query){
                $success="Password changed sucessfully";
            }
            else {
                echo "something went wrong";
            }  
        }
    }
        


?>
<?php

require('render1.php');

?>

<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-th"></span>
                        Change password   
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 separator social-login-box"> <br>
                           <img alt="" class="img-thumbnail" src="https://bootdey.com/img/Content/avatar/avatar1.png">                        
                        </div>
                        <div style="margin-top:80px;" class="col-xs-6 col-sm-6 col-md-6 login-box">
                        <form method="POST">
                         <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                              <input class="form-control" type="password" placeholder="New Password" name="newpassword">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-log-in"></span></div>
                              <input class="form-control" type="password" placeholder="Confirm New Password" name="confirmnewpassword">
                              
                            </div><br>
                            <?php echo $success; echo $error; echo $error2;  ?>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6"></div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <button class="btn icon-btn-save btn-success" type="submit" name="submitnewpassword">
                            <span class="btn-save-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>save</button>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php




require('render2.php');

?>