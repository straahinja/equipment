<?php
session_start();
if (!isset($_SESSION['em'])) {
  header('Location: index.php');
}
?>



<?php
    error_reporting( E_ALL & ~E_NOTICE ^ E_DEPRECATED );
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>User</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
 
</head>


<?php
require('credentials.php');

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="pocetna.php">
        <div class="sidebar-brand-icon ">
          <i class="fas fa-user"></i>

        </div>
        <div class="sidebar-brand-text mx-3">User Panel<sup></sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <!-- <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li> -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Item</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Select options:</h6>
            <a class="collapse-item" href="showitems.php">Request Item</a>
            <a class="collapse-item" href="mystuff.php">My Items</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Request Item from Users</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Select options:</h6>
            <a class="collapse-item" href="borrow.php">Borrow Item</a>
            <!-- <a class="collapse-item" href="#">Test</a> -->
            
          </div>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Requests</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

            <a class="collapse-item" href="showuserrequest.php">Show requests</a>
             <!-- <a class="collapse-item" href="#">Example</a>  -->
            <!-- <a class="collapse-item" href="#">Example</a> -->

          </div>
          
        </div>
        
      </li>
      




      <!-- Divider -->
      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseClock" aria-expanded="true" aria-controls="collapseClock">
        <i class="fas fa-clock"></i>
          <span>Time Tracking</span>
        </a>
        <div id="collapseClock" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

            <a class="collapse-item" href="/equipment/timer_test/timer_test.php">TEST!</a>
             <!-- <a class="collapse-item" href="#">Example</a>  -->
            <!-- <a class="collapse-item" href="#">Example</a> -->

          </div>
          
        </div>
        
      </li>

      <hr class="sidebar-divider">

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBook" aria-expanded="true" aria-controls="collapseBook">
        <i class="fas fa-clock"></i>
          <span>Books</span>
        </a>
        <div id="collapseBook" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

            <a class="collapse-item" href="/equipment/user/books.php">All books</a>
            <a class="collapse-item" href="/equipment/user/my-books.php">My books</a>
            <a class="collapse-item" href="/equipment/user/request-books-user.php">Request book from user</a>
            <!-- <a class="collapse-item" href="/equipment/user/showbookrequests">Show requests from user</a> -->
             <!-- <a class="collapse-item" href="#">Example</a>  -->
            <!-- <a class="collapse-item" href="#">Example</a> -->

          </div>
          
        </div>
        
      </li>


      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAbsence" aria-expanded="true" aria-controls="collapseAbsence">
          <i class="fas fa-running"></i>
          <span>Absence</span>
        </a>
        <div id="collapseAbsence" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

            <a class="collapse-item" href="absence.php">Request absence</a>
             <a class="collapse-item" href="approvedAbsence.php">Approved Absence</a> 
            <!-- <a class="collapse-item" href="#">Example</a> -->

          </div>
          
        </div>
        
      </li>

      


      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
        
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">

              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['em'] ?></span>
                <img src="https://img.icons8.com/color/48/000000/user.png" />
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <!-- <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a> -->
                <a class="dropdown-item" href="changepassword.php">
                  <i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>
                  Change Password
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
     
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <!-- <h1 class="h3 mb-0 text-gray-800">Statistics</h1> -->
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>

          <!-- Content Row -->
          <div class="row">     
            <!-- Content -->
  <div class="container">
      <!--Grid row-->
      <div class="row wow fadeIn">
          <!--Grid column-->
          <div class="col-md-6 col-xl-5 mb-4 mx-auto">
              <!--Card-->
              <div class="card">
                  <!--Card content-->
                  <div class="card-body">    
                      <!-- Form -->
                      <form method="POST">
                          <!-- Heading -->
                          <h3 class="dark-grey-text text-center">
                              <strong>Absence Request</strong>
                          </h3>
                          <hr>

                          <div class="md-form">
                            <label for="form3">Date From</label>
                            <i class="fas fa-calendar-alt prefix active"></i>
                            <input style="width: 100%;" type="text" readonly id="datepicker" name="datepicker" ></p> 
                          </div>

                          <div class="md-form">
                            <label for="form4">Date To</label>
                            <i class="fas fa-calendar-alt prefix active"></i>
                            <input style="width: 100%;" type="text" readonly id="datepicker2" name="datepicker2" ></p> 
                          </div>

                          <input type='hidden' name='status' value='requested'>

                          <i class="fas fa-keyboard prefix active"></i>
                          <select class="browser-default custom-select" name="type">
                              <option selected>Type of time off</option>
                              <option value="Sick">Sick</option>
                              <option value="Vacation">Vacation</option>
                              <option value="TimeOffWithoutPay">Time off without pay</option>
                              <option value="Military">Military</option>
                              <option value="JuryDuty">Jury Duty</option>
                              <option value="MaternityPaternity">Maternity/Paternity</option>
                              <option value="Other">Other</option>
                          </select>

                          <!-- Message -->
                          <div class="form-group">
                          <i class="fas fa-comment-dots prefix active"></i>
                              <textarea style="width: 100%; margin-top:10px;" class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" placeholder="Message" name="message"></textarea>
                          </div>


                          <div class="text-center">
                              <button style="margin-top: 10px;" type="submit" class="btn btn-success" name="dodajdugme">Submit</button>
                              <hr>
                          </div>
                            
                      </form>

                      <?php
                          require('credentials.php');
                          if(isset($_POST['dodajdugme'])){
                            $datefrom=$_POST['datepicker']; //datefrom 
                            $dateto=$_POST['datepicker2'];
                            $type = $_POST['type'];
                            $messageArea=$_POST['message'];
                            $status=$_POST['status'];

                            $datefromERROR="";
                            $datetoERROR="";
                            $typeERROR="";
                            $messageERROR="";

                            require('connection.php');

                            if(empty($datefrom)){
                              $datefromERROR="Date is required";
                            }
                          
                            if(empty($dateto)){
                              $datetoERROR="Date is required";
                            }

                            if(empty($type)){
                              $typeERROR="Type is required";
                            }

                            if(empty($messageArea)){
                              $messageERROR="Message is required";
                            }
                          

                            if(empty($datefromERROR) && empty($datetoERROR) && empty($typeERROR) && empty($messageERROR)){

                              $query= mysqli_query($conn, "INSERT INTO absencerequest (fullname, email, datefrom, dateto, type, message,status) VALUES ('".$ime."', '".$email."','".$datefrom."','".$dateto."','".$type."','".$messageArea."','".$status."')");

                              if($query){
                                  $uspesno="successfully requested";
                              } else {
                                echo "something went wrong!";
                              }
                            }
                          }
                        ?>
                      
                      <!-- Form -->
                        <center>  <label style="color:red; letter-spacing:1px; font-weight:bold;"><?php echo"$datefromERROR <br> $datetoERROR <br> $typeERROR <br> $messageERROR"?></label></center>
                        <!-- <center>  <label style="color:green; letter-spacing:1px; font-weight:bold;"><?php echo" <br>$uspesno"?></label></center> -->
                  </div>
              </div>
              <!--/.Card-->
          </div>
          <!--Grid column-->
      </div>
      <!--Grid row-->
  </div>
 
</div>


        <div class="row">
            <!-- Content Row -->

            <div class="row">

              <!-- Pie Chart -->
              <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">

                </div>
              </div>
            </div>

            <!-- Content Row -->
            <div class="row">

              <div class="col-lg-6 mb-4">

                <!-- Approach -->
                <div class="card shadow mb-4">

                </div>
              </div>

            </div>
            <!-- /.container-fluid -->

          </div>
          <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->
        <!-- Footer -->

      </div>
      
      <!-- End of Page Wrapper -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; UNDERIT 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
      </a>

      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn btn-primary" href="logout.php">Logout</a>
            </div>
          </div>
        </div>
      </div>


      <!-- Bootstrap core JavaScript-->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <!-- Core plugin JavaScript-->
      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- Custom scripts for all pages-->
      <script src="js/sb-admin-2.min.js"></script>

      <!-- Page level plugins -->
      <script src="vendor/chart.js/Chart.min.js"></script>

      <!-- Page level custom scripts -->
      <script src="js/demo/chart-area-demo.js"></script>
      <script src="js/demo/chart-pie-demo.js"></script>

      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

      <script>
      $( function() {
        $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
      } );
    </script>

    <script>
      $( function() {
        $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
      } );
    </script>

</body>

</html>